" VIMRC Personnalisé

set mouse-=a
syntax enable
set number

" Afficher les commandes incomplètes, la ligne curseur et augmente les niveaux hiso et undo
set showcmd
set cursorline
set lazyredraw
set history=100
set undolevels=150

" Tout ce qui concerne la recherche. Incrémentale
" avec un highlight. Elle prend en compte la
" différence entre majuscule/minuscule.
set incsearch
set noignorecase
set infercase

" Quand la rechercher atteint la fin du fichier, pas
" la peine de la refaire depuis le début du fichier
set hlsearch
set ttyfast

if has("autocmd")
    autocmd! bufwritepost .vimrc source ~/.vimrc
endif
